# README

This project focuses on developing an ACME editor 'clone' for LOVE2D for all supported platforms. However, until a stable release the support restricts to MacOS and iOS only.

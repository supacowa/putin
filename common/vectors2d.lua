--[[
Copyright (c) 2010-2013 Matthias Richter

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

Except as contained in this notice, the name(s) of the above copyright holders
shall not be used in advertising or otherwise to promote the sale, use or
other dealings in this Software without prior written authorization.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

http://hump.readthedocs.io/en/latest/index.html
--]]


local sqrt, cos, sin, atan2 = math.sqrt, math.cos, math.sin, math.atan2
local isNum = require("common.helpers").isNum
local isTable = require("common.helpers").isTable

local vec2 = class()


function vec2:init(x, y) -- default constructor
    self.y = y or x or 0
    self.x = x or 0
end


function vec2:fromPolar(angle, radius) -- another type constructor (e.g. local vector = vec2():fromPolar(90, 64))
    return vec2(cos(angle) * radius, sin(angle) * radius)
end


function vec2:isVector()
    return isTable(self) and isNum(self.x) and isNum(self.y)
end


function vec2:clone()
    return vec2(self.x, self.y)
end


function vec2:unpack()
    return self.x, self.y
end


function vec2:__tostring()
    return "("..tonumber(self.x)..", "..tonumber(self.y)..")"
end


function vec2.__unm(a)
    return vec2(-a.x, -a.y)
end


function vec2.__add(a, b)
    assert(vec2.isVector(a) and vec2.isVector(b), "Add: wrong argument types (<vector> expected)")
    return vec2(a.x + b.x, a.y + b.y)
end


function vec2.__sub(a, b)
    assert(vec2.isVector(a) and vec2.isVector(b), "Sub: wrong argument types (<vector> expected)")
    return vec2(a.x - b.x, a.y - b.y)
end


function vec2.__mul(a, b)
    if isNum(a) then
        return vec2(a * b.x, a * b.y)
    elseif isNum(b) then
        return vec2(b * a.x, b * a.y)
    else
        assert(vec2.isVector(a) and vec2.isVector(b), "Mul: wrong argument types (<vector> or <number> expected)")
        return a.x * b.x + a.y * b.y
    end
end


function vec2.__div(a, b)
    assert(vec2.isVector(a) and type(b) == "number", "wrong argument types (expected <vector> / <number>)")
    return vec2(a.x / b, a.y / b)
end


function vec2.__eq(a, b)
    return a.x == b.x and a.y == b.y
end


function vec2.__lt(a, b)
    return a.x < b.x or (a.x == b.x and a.y < b.y)
end


function vec2.__le(a, b)
    return a.x <= b.x and a.y <= b.y
end


function vec2.permul(a, b)
    assert(vec2.isVector(a) and vec2.isVector(b), "permul: wrong argument types (<vector> expected)")
    return vec2(a.x * b.x, a.y * b.y)
end


function vec2:toPolar()
    return vec2(atan2(self.x, self.y), self:len())
end


function vec2:len2()
    return self.x * self.x + self.y * self.y
end


function vec2:len()
    return sqrt(self.x * self.x + self.y * self.y)
end


function vec2.dist(a, b)
    assert(vec2.isVector(a) and vec2.isVector(b), "dist: wrong argument types (<vector> expected)")
    local dx = a.x - b.x
    local dy = a.y - b.y
    return sqrt(dx * dx + dy * dy)
end


function vec2.dist2(a, b)
    assert(vec2.isVector(a) and vec2.isVector(b), "dist: wrong argument types (<vector> expected)")
    local dx = a.x - b.x
    local dy = a.y - b.y
    return (dx * dx + dy * dy)
end


function vec2:normalizeInplace()
    local len = self:len()
    if len > 0 then
        self.x, self.y = self.x / len, self.y / len
    end
    return self
end


function vec2:normalized()
    return self:clone():normalizeInplace()
end


function vec2:rotateInplace(phi)
    local c, s = cos(phi), sin(phi)
    self.x, self.y = c * self.x - s * self.y, s * self.x + c * self.y
    return self
end


function vec2:rotated(phi)
    local c, s = cos(phi), sin(phi)
    return vec2(c * self.x - s * self.y, s * self.x + c * self.y)
end


function vec2:perpendicular()
    return vec2(-self.y, self.x)
end


function vec2:projectOn(v)
    assert(vec2.isVector(v), "invalid argument: cannot project vector on " .. type(v))
    -- (self * v) * v / v:len2()
    local s = (self.x * v.x + self.y * v.y) / (v.x * v.x + v.y * v.y)
    return vec2(s * v.x, s * v.y)
end


function vec2:mirrorOn(v)
    assert(vec2.isVector(v), "invalid argument: cannot mirror vector on " .. type(v))
    -- 2 * self:projectOn(v) - self
    local s = 2 * (self.x * v.x + self.y * v.y) / (v.x * v.x + v.y * v.y)
    return vec2(s * v.x - self.x, s * v.y - self.y)
end


function vec2:cross(v)
    assert(vec2.isVector(v), "cross: wrong argument types (<vector> expected)")
    return self.x * v.y - self.y * v.x
end


function vec2:trimInplace(maxLen) -- reference: http://blog.signalsondisplay.com/?p=336
    local s = maxLen * maxLen / self:len2()
    s = (s > 1 and 1) or math.sqrt(s)
    self.x, self.y = self.x * s, self.y * s
    return self
end


function vec2:angleTo(other)
    if other then
        return atan2(self.y, self.x) - atan2(other.y, other.x)
    end
    return atan2(self.y, self.x)
end


function vec2:trimmed(maxLen)
    return self:clone():trimInplace(maxLen)
end


return vec2
-- (c) 2018 kontakt@herrsch.de

-- TODO use __pow as appending shortcut (like Amulet.xyz)?
-- TODO think about how to implement scene graph, including shaders (also like Amulet)...

scene = {}

node = class()

function node:init(tag, parent) -- create new node class
    assert(tag, "node must have a tag name")
    self.tag = tag
    return self
end

function node:prepend(new_node) -- append self onto new node
end

function node:append(new_node) -- append new node onto self
end

function node:find(tag) -- find child node by tag name
end

function node:count() -- count all children nodes
end

function node:list() -- list all children nodes
end

function node:action(routine)
end

function node:draw() -- self drawing routine
    self:action()
end


audio = node() -- action node

function audio:init(source)
    assert(type(source) == "string", "source path must be a valid string")
    
    local src

    self.source = get(function()
    end)

    self.action = get(this, function()

    end)

    return self:action()
end
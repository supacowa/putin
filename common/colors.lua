-- Prior versions of LOVE2D (<0.11.0) used RGBA values in range of [0-255] but since then values must be passed in as normalized values in range of [0-1].
-- It is easier to work with standard [0-255] values, so this convenience class allows to work with both formats by providing methods to access any format.

-- (c) 2018 kontakt@herrsch.de

-- TODO add methods for mixing/adding/substracting colors and converting color spaces if needed

local clamp = function(n, low, high) return math.min(math.max(low or 0, n), high or 1) end
local Color = class()

function Color:init(...)
    -- set defaults
    local varg = {...} -- r, g, b, a
    local max = 255
    local red = varg[1] or max
    local green = not varg[3] and red or (varg[2] or red)
    local blue = varg[3] or red
    local alpha = not varg[3] and (varg[4] or varg[2] or max) or (varg[4] or max)
    local normal = {}

    self.r = get(function() return red end)
    self.g = get(function() return green end)
    self.b = get(function() return blue end)
    self.a = get(function() return alpha end)

    self.R = get(function() return red * max end)
    self.G = get(function() return green * max end)
    self.B = get(function() return blue * max end)
    self.A = get(function() return alpha * max end)

    self.unpack = function()
        return red, green, blue, alpha -- normalized
    end

    self.normalized = function(this, r, g, b, a)
        if not r or not g or not b or not a then
            return this:unpack()
        end
    
        r = clamp(r, 0, 255)
        g = clamp(g, 0, 255)
        b = clamp(b, 0, 255)
        a = clamp(a, 0, 255)
    
        return -- normalize if needed
            r > 1 and r/255 or r,
            g > 1 and g/255 or g,
            b > 1 and b/255 or b,
            a > 1 and a/255 or a
    end
    
    self.getRGBA = function(this)
        return this.R, this.G, this.B, this.A
    end

    self.setRGBA = function(this, r, g, b, a)
        red, green, blue, alpha = this:normalized(r, g, b, a)
    end

    self:setRGBA(red, green, blue, alpha)
end

return Color

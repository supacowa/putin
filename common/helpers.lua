-- This are small, helpful, GLOBAL functions for manipulating different data-types
-- When you extend this collection, make your method `local` and don't forget to register it it in the `return` statement at the bottom of this file!

-- NOTE we try to avoid monkey-paching, so it will only work on local basis...

-- (c) 2018 kontakt@herrsch.de


local M = {}







-- check if given value is a integer or float (lua number) value
function M.isNum(v)
    return v and type(v) == "number"
end








-- Check if given number is an integer (not a float)
function M.isInt(n)
    return n and isNumber(n) and n == math.floor(n)
end








-- Check if given value is of type table
function M.isTable(value)
    return value and type(value) == "table"
end








function M.isFunc(value)
    return value and type(value) == "function"
end









-- If a variable is nil then choose default-boolean otherwise take variables boolean value (which is the opposite of default)
-- NOTE that `return <bool> or <default bool>` will not always work because it would use <default bool> on both <nil> and <false> but we want only override the <nil>!

function M.defaultToBoolean(bool, default)
    return type(bool) == "nil" and default or bool
end









-- Clamps a number to within a certain range [n, M]
function M.clamp(n, low, high)
    return math.min(math.max(low or 0, n), high or 1)
end








-- Map value from one range to another
function M.remap(val, a1, a2, b1, b2)
    return b1 + (val-a1) * (b2-b1) / (a2-a1)
end











-- Linear interpolation between two numbers
function M.lerp(from, to, fraction)
    local f = clamp(fraction)
    return (1 - f) * from + f * to
end



function M.lerp2(from, to, fraction)
    return from + (to - from) * clamp(fraction)
end


-- Cosine interpolation between two numbers
function M.cerp(from, to, fraction)
    local f = (1 - math.cos(clamp(fraction) * math.pi)) * .5
    return from * (1 - f) + to * f
end

















-- NOTE both of these functions are framework/engine dependent

-- Get the position of a point on screen as a dynamic percentage value [0-1]
function M.pixelToPercent(point_x, point_y)
    local width, height = love.window.getMode()
    return point_x / width, point_y / height
end


-- Get the pixel position on screen from a percentage value [0-1]
function M.percentToPixel(dyn_x, dyn_y)
    local width, height = love.window.getMode()
    return width * dyn_x, height * dyn_y
end










return M

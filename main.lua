local pretty = require("common.tables").tostring

love.graphics.setDefaultFilter("nearest", "nearest")
love.keyboard.setKeyRepeat(true)

local loverboy = love.graphics.newImage "gfx/loverboy_back.jpg"
local lover_x, lover_y = 125, 125

local moonshine = require "common.moonshine"
local shader_pipeline = moonshine(moonshine.effects.scanlines)
    .next(moonshine.effects.crt)
    .next(moonshine.effects.desaturate)
    .next(moonshine.effects.filmgrain)

-- TODO let scanlines have its own crt effect that only affects y but NOT x
-- then overlay that scanlines over the crt effect of the scene (which has wrapping on x and y)
-- This prevents crossing choppy lines on the scanlines

shader_pipeline.crt.distortionFactor = {1.06, 1.07}
shader_pipeline.crt.feather = .02
shader_pipeline.desaturate.strength = .3
shader_pipeline.scanlines.width = 1.5
shader_pipeline.scanlines.thickness = .25
shader_pipeline.scanlines.opacity = .3

function love.update(delta)
    shader_pipeline.filmgrain.size = math.random(2, 8)
    shader_pipeline.filmgrain.opacity = math.sin(math.random()) * .1
end

function love.draw()
    shader_pipeline(function()
        love.graphics.setColor(127, 70, 127, 255)
        love.graphics.rectangle("fill", 1, 1, love.graphics.getWidth(), love.graphics.getHeight())
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.draw(loverboy, lover_x, lover_y, 0, 3, 3)
    end)
end

function love.keypressed(key, code)
    local step = 8
    if     key == "up"    then lover_y = lover_y - step
    elseif key == "down"  then lover_y = lover_y + step
    elseif key == "left"  then lover_x = lover_x - step
    elseif key == "right" then lover_x = lover_x + step end
end
require "common.classes"

function love.conf(g)
    g.version = "0.10.2" -- min love2d version
    g.identity = "unknown_savedir" -- save directory name
    g.console = false -- Windows only
    g.externalstorage = false -- save files (and read from the save directory) in external storage on Android
    g.accelerometerjoystick = true -- accelerometer on iOS and Android is exposed as joystick
    g.gammacorrect = false

    g.window.vsync = false
    g.window.msaa = 8
    g.window.display = 2 -- active display index
    g.window.highdpi = false -- retina

    g.window.icon = nil -- path
    g.window.title = "unknown_title"
    g.window.x = nil
    g.window.y = nil
    g.window.width = 640
    g.window.height = 480
    g.window.minwidth = 160
    g.window.minheight = 120
    g.window.borderless = false
    g.window.resizable = true
    g.window.fullscreen = false
    g.window.fullscreentype = "desktop"
 
    g.modules.video = true
    g.modules.audio = true
    g.modules.sound = true
    g.modules.window = true
    g.modules.image = true
    g.modules.event = true
    g.modules.system = true
    g.modules.thread = true
    g.modules.physics = true
    g.modules.math = true
    g.modules.timer = true
    g.modules.graphics = true
    g.modules.keyboard = true
    g.modules.joystick = true
    g.modules.mouse = true
    g.modules.touch = true
end
